// Conditional Statements


// If statement

// let numA = -1

// /*
// 	Syntax:
// 	if(condition is true){
// 		statement / code block
// 	};

//  */

// if(numA < 0){
// 	console.log('Hi, I am less than 0');
// };

// console.log(numA < 0);



// if(numA < 0){
// 	console.log("Hi I am from numA is 0")
// };

// console.log(numA < 0);


// let city = "New York"

// if(city === "New York"){
// 	console.log("Welcome to New York City!");
// };

// Else If Clause

// let numA = 0;
// let numH = 1;

// if (numA < 0){
// 	console.log("Hello");
// } else if (numH > 0) {
// 	console.log("World");
// };
// numA = 1
// if(numA > 0){
// 	console.log("Hello");
// } else if (numH > 0){
// 	console.log("World");
// }

// city = "Tokyo";

// if(city === "New York"){
// 	console.log("Welcome to New York City");
// } else if(city === "Tokyo"){
// 	console.log("Welcome to Tokyo!");
// };



// Else Statement
let numA = 0;
let numH = 1;

if(numA < 0){
	console.log("Hello");
} else if(numH === 0){
	console.log("World");
} else {
	console.log("Again");
};

// else cannot run without if


// Conditional Statement inside the function
let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return 'Not a typhoon yet';

	} else if(windSpeed <= 61){
		return 'Tropical depression detected';

	} else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';

	} else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	} else {
		return 'Typhoon detected';
	};
};

message = determineTyphoonIntensity(65);
console.log(message);

if(message == "Tropical storm detected"){
	console.warn(message);
};

// Truthy and Falsy

/* Truthy Examples
	1. Boolean Value of True

*/

if(true){
	console.log("Boolean true is Truthy");
};

if(1){
	console.log("1 is Truthy");
};

if([]){
	console.log("Blank array is Truthy");
};

let isAdmin = true

if(isAdmin === true){
	console.log("Admin is true");
}; //instead of doing this

// you can do thus
if(isAdmin){
	console.log("Admin is true");
};

// Falsy Examples

if(false){
	console.log("This is Falsy");
};

if(0){
	console.log("0 is Falsy");
};

if(undefined){
	console.log("undefined is Falsy");
};

//it will not run

// Ternary Operators
	/* 
		(expression) ? ifTrue : ifFalse;

	*/

// Single Statement Execution

let ternaryResult = (1 < 18) ? true : false
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement Execution

let name;

function isOfLegalAge(){
	name = "Mark";
	return 'You are of age limit'
};

function isUnderAge(){
	name = "Michelle";
	return 'You are under the age limit'
};

	// parseInt - converting a string to a number
let age = parseInt(prompt('What is your age'));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log('Result of Ternary Operator in functions: ' + legalAge + ', ' + name);


// Switch Statement

/*
	Syntax:
	switch (expression){
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
*/

let day = prompt ("What day of the week is it today ").toLowerCase();
console.log(day);

	switch(day){
		case 'monday' :
		console.log('Color of the day is red');
		break;

		case 'tuesday' :
		console.log('Color of the day is blue');
		break;

		case 'wednessday' :
		console.log('Color of the day is yellow');
		break;

		case 'thursday' :
		console.log('Color of the day is green');
		break;

		case 'friday' :
		console.log('Color of the day is orange');
		break;

		case 'saturday' :
		console.log('Color of the day is pink');
		break;

		case 'sunday' :
		console.log('Color of the day is white');
		break;

		default:
		console.log("Please input a valid day");
		break;
	};


// Try-Catch-Finally

function showIntensityAlert(windSpeed){

	try {
		console.lg("Hi I am an error")
		alerat(determineTyphoonIntensity(windSpeed));

	} catch (error) {
		console.log(typeof error); //object
		console.warn(error.message);

	} finally {
		alert("Intensity updates will show new alert");
	};
};

showIntensityAlert(65);